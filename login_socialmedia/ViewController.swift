//
//  ViewController.swift
//  login_socialmedia
//
//  Created by Apple on 5/4/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FacebookShare
import FBSDKLoginKit
import GoogleSignIn

class ViewController: UIViewController  {

    @IBOutlet weak var googleBt: UIButton!
    var googleSignIn = GIDSignIn.sharedInstance()
    var googleId = ""
    var googleIdToken = ""
    var googleFirstName = ""
    var googleLastName = ""
    var googleEmail = ""
    var googleProfileURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func signinFbTapped(_ sender: UIButton) {
        fbLogin()
    }
    
    @IBAction func signinGoogleTapped(_ sender: AnyObject) {
       self.googleAuthLogin()
    }
    
    @IBAction func signinTwitterTapped(_ sender: UIButton) {
    }
    
    @IBAction func signinInstagramTapped(_ sender: UIButton) {
    }

    func fbLogin() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions:[ .publicProfile, .email], viewController: self) { loginResult in
        
        switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login process.")
            case .success( _, _, _):
                print("Logged in!")
                self.getFBUserData()
               }
           }
       }
    
    func getFBUserData() {
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    print(result!)
                    print(dict)
                    let picutreDic = dict as NSDictionary
                    let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                    let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                    let finalURLofdp = tmpURL2.object(forKey: "url") as! String
                    
                    let nameOfUser = picutreDic.object(forKey: "name") as! String
                    print(nameOfUser)
                    
                    var tmpEmailAdd = ""
                    if let emailAddress = picutreDic.object(forKey: "email") {
                        tmpEmailAdd = emailAddress as! String
                        print(tmpEmailAdd)
                    }
                    else {
                        var usrName = nameOfUser
                        usrName = usrName.replacingOccurrences(of: " ", with: "")
                        tmpEmailAdd = usrName+"@facebook.com"
                    }
                }
                print(error?.localizedDescription as Any)
            })
        }
    }
    
    func googleAuthLogin() {
           self.googleSignIn?.presentingViewController = self
           self.googleSignIn?.clientID = "456629253696-hp3pun0qp386priav1ngo5n5bcb6i302.apps.googleusercontent.com"
           self.googleSignIn?.delegate = self
           self.googleSignIn?.signIn()
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailGoogleData" {
            let data = segue.destination as! GoogleDetailsVC
            data.googleId = self.googleId
            data.googleIDToken = self.googleIdToken
            data.googleFirstName = self.googleFirstName
            data.googleLastName = self.googleLastName
            data.googleEmail = self.googleEmail
            data.googleProfilePicURL = self.googleProfileURL
        }
    }
}

extension ViewController : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else {
            print("Uh oh. The user cancelled the Google login.")
            return
        }
        let userId = user.userID ?? ""
        print("Google User ID: \(userId)")
        self.googleId = userId
        
        let userIdToken = user.authentication.idToken ?? ""
        print("Google ID Token: \(userIdToken)")
        self.googleIdToken = userIdToken
        
        let userFirstName = user.profile.givenName ?? ""
        print("Google User First Name: \(userFirstName)")
        self.googleFirstName = userFirstName
        
        let userLastName = user.profile.familyName ?? ""
        print("Google User Last Name: \(userLastName)")
        self.googleLastName = userLastName
        
        let userEmail = user.profile.email ?? ""
        print("Google User Email: \(userEmail)")
        self.googleEmail = userEmail
        
        let googleProfilePicURL = user.profile.imageURL(withDimension: 150)?.absoluteString ?? ""
        print("Google Profile Avatar URL: \(googleProfilePicURL)")
        self.googleProfileURL = googleProfilePicURL
        
        self.performSegue(withIdentifier: "detailGoogleData", sender: self)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Call your backend server to delete user's info after they requested to delete their account
    }
}
