//
//  GoogleDetailsVC.swift
//  login_socialmedia
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit

class GoogleDetailsVC: UIViewController {
      @IBOutlet var googleIdLabel: UILabel!
      @IBOutlet var googleFirstNameLabel: UILabel!
      @IBOutlet var googleLastNameLabel: UILabel!
      @IBOutlet var googleEmailLabel: UILabel!
      @IBOutlet var googleProfilePicUrlLabel: UILabel!
      @IBOutlet var googleIDTokenLabel: UILabel!

      var googleId = ""
      var googleFirstName = ""
      var googleLastName = ""
      var googleEmail = ""
      var googleProfilePicURL = ""
      var googleIDToken = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        googleIdLabel.text = googleId
        googleFirstNameLabel.text = googleFirstName
        googleLastNameLabel.text = googleLastName
        googleEmailLabel.text = googleEmail
        googleProfilePicUrlLabel.text = googleProfilePicURL
        googleIDTokenLabel.text = googleIDToken
    }

}
